package ru.babaninnv.otus.quicksorts.quick;

public class QuickSort {
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int low, int high) {
        if (low < high) {
            int partPosition = partition(array, low, high);
            sort(array, low, partPosition - 1);
            sort(array, partPosition + 1, high);
        }
    }

    public int partition(int[] array, int low, int high) {
        // сначала определяем опорное значение
        int pivot = array[high];

        // определяем наименьший индекс, перед которым
        // будем размещать все значения меньше опорного
        int i = low - 1;

        // пробегаемся от меньшего индекса к большему,
        // перемещая значения влево, если они меньше опорного
        for (int j = low; j <= high - 1; j++) {
            if (array[j] < pivot) {
                i++;
                swap(array, i, j);
            }
        }

        // меняем местами опорное занчение и следующее значение за найденным меньшим чем опорное, таким образом,
        // помещая опорное значение между теми которые его меньше и которые его больше
        swap(array, i + 1, high);
        return i + 1;
    }

    private void swap(int[] array, int left, int right) {
        int tmp = array[left];
        array[left] = array[right];
        array[right] = tmp;
    }
}
