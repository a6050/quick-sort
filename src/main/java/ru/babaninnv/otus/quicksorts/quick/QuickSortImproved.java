package ru.babaninnv.otus.quicksorts.quick;

public class QuickSortImproved {
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int low, int high) {
        if (low < high) {
            int partPosition = partition(array, low, high);
            sort(array, low, partPosition - 1);
            sort(array, partPosition + 1, high);
        }
    }

    public int partition(int[] array, int low, int high) {
        int pivot = array[high];

        int a = low - 1;

        for (int i = low; i <= high; i++) {
            if (array[i] <= pivot) {
                swap(array, ++a, i);
            }
        }

        return a;
    }

    private void swap(int[] array, int left, int right) {
        int tmp = array[left];
        array[left] = array[right];
        array[right] = tmp;
    }
}
