package ru.babaninnv.otus.quicksorts.merge;

public class MergeSort {
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int leftI, int rightI) {
        if (rightI > leftI) {
            int middle = leftI + (rightI - leftI) / 2;
            sort(array, leftI, middle);
            sort(array, middle + 1, rightI);
            merge(array, leftI, middle, rightI);
        }
    }

    private void merge(int[] array, int leftI, int middle, int rightI) {
        int[] leftArray = new int[middle - leftI + 1];
        int[] rightArray = new int[rightI - middle];


        for (int i = 0; i < leftArray.length; ++i) {
            leftArray[i] = array[leftI + i];
        }

        for (int j = 0; j < rightArray.length; ++j) {
            rightArray[j] = array[middle + 1 + j];
        }

        int i = 0;
        int j = 0;
        int k = leftI;

        while (i < leftArray.length && j < rightArray.length) {
            if (leftArray[i] <= rightArray[j]) {
                array[k] = leftArray[i];
                i++;
            }
            else {
                array[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while (i < leftArray.length) {
            array[k++] = leftArray[i++];
        }
        while (j < rightArray.length) {
            array[k++] = rightArray[j++];
        }
    }
}
