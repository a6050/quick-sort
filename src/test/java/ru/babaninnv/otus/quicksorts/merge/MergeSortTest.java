package ru.babaninnv.otus.quicksorts.merge;

import ru.babaninnv.otus.quicksorts.test.SortingTestFactory;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortTest extends SortingTestFactory {

    @Override
    public void runSort(int[] array) {
        new MergeSort().sort(array);
    }
}