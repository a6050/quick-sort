package ru.babaninnv.otus.quicksorts.quick;

import ru.babaninnv.otus.quicksorts.test.SortingTestFactory;

class QuickSortTest extends SortingTestFactory {

    @Override
    public void runSort(int[] array) {
        new QuickSort().sort(array);
    }
}