package ru.babaninnv.otus.quicksorts.quick;

import ru.babaninnv.otus.quicksorts.test.SortingTestFactory;

class QuickSortImprovedTest extends SortingTestFactory {

    @Override
    public void runSort(int[] array) {
        new QuickSortImproved().sort(array);
    }
}